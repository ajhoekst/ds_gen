from .common import add_common_args
from .common import handle_common_args
from .common import check_arg_natural_number
