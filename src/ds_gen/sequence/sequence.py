import functools

from .Array import add_array_subparsers
from .Vector import add_vector_subparsers

def add_sequence_subparsers(add_parent_args, parent_handler, parent_subparsers):
    # Add parser
    parser = parent_subparsers.add_parser('sequence')
    sequence_subparsers = parser.add_subparsers()

    # Define subparsers
    subparsers = [
        add_array_subparsers,
        add_vector_subparsers
    ]

    # Add subparsers
    for subparser in subparsers:
        functools.partial(
            subparser,
            functools.partial(add_sequence_args, add_parent_args),
            functools.partial(handle_sequence_args, parent_handler),
            sequence_subparsers
        )()

def add_sequence_args(add_parent_args, parser):
    # Add parent arguments
    add_parent_args(parser)

    # Add sequence-related arguments
    parser.add_argument(
        'type',
        nargs='?',
        default='int',
        type=str,
        help='The type being stored in the sequence'
    )
    parser.add_argument(
        'type_header',
        nargs='?',
        default=None,
        type=str,
        help='The header file declaring the type being stored in the sequence'
    )


def handle_sequence_args(parent_handler, args, template):
    # Process parent arguments
    updated_template = parent_handler(args, template)

    # Replace type tags
    updated_template = updated_template.replace(
        '%%type%%',
        args.type
    )
    
    # Replace includes tag
    if args.type_header:
        updated_template = updated_template.replace(
            '%%includes%%',
            '\n#include "' + args.type_include + '"\n'
        )
    else:
        updated_template = updated_template.replace(
            '%%includes%%',
            ''
        )
    
    return updated_template
