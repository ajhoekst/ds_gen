import functools
import os

from ..common import check_arg_natural_number

def add_array_subparsers(add_parent_args, parent_handler, parent_subparsers):
    # Add parser
    parser = parent_subparsers.add_parser('array')
    # Add arguments
    add_array_args(add_parent_args, parser)
    # Set command defaults
    parser.set_defaults(
        file=__file__,
        func=functools.partial(handle_array_args, parent_handler)
    )

def add_array_args(add_parent_args, parser):
    # Add parent arguments
    add_parent_args(parser)

    # Add array-related arguments
    parser.add_argument(
        '-c',
        '--capacity',
        nargs='?',
        default=16,
        type=functools.partial(check_arg_natural_number, 'Capacity'),
        help='Capacity of the array'
    )

def handle_array_args(parent_handler, args, template):
    # Replace sequence-related tags
    updated_template = parent_handler(args, template)

    # Replace array-specific tags
    updated_template = updated_template.replace(
        '%%capacity%%',
        str(args.capacity)
    )

    return updated_template
