#include "%%scope%%_Vector_gen.h"

// Function definitions
// Allocation
%%scope%%_Vector_error %%scope%%_Vector_init(%%scope%%_Vector* vector, void* (*allocator)(size_t size))
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        vector->capacity = %%capacity%%;
        vector->size = 0;
        vector->data = (%%type%%*) allocator(%%capacity%% * sizeof(%%type%%));

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
// Accessors
%%scope%%_Vector_error %%scope%%_Vector_at(%%scope%%_Vector* vector, int index, %%type%%* reference)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        if (index >= 0 && index < vector->size)
        {
            reference = &vector->data[index];

            toReturn = %%scope%%_Vector_eOk;
        }
        else { toReturn = %%scope%%_Vector_eRange; }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_front(%%scope%%_Vector* vector, %%type%%* reference)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        reference = &vector->data[0];

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_back(%%scope%%_Vector* vector, %%type%%* reference)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        reference = &vector->data[vector->size - 1];

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
// Capacity
%%scope%%_Vector_error %%scope%%_Vector_empty(%%scope%%_Vector* vector, int* result)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        *result = vector->size == 0;

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_size(%%scope%%_Vector* vector, int* size)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        toReturn = vector->size;

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
// int %%scope%%_Vector_maxSize(%%scope%%_Vector* vector);
%%scope%%_Vector_error %%scope%%_Vector_reserve(%%scope%%_Vector* vector, int size)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        if (size >= 0)
        {
            if (!%%scope_upper%%_VECTOR_MAX_SIZE || size <= %%scope_upper%%_VECTOR_MAX_SIZE)
            {
                if (size > vector->size)
                {
                    // Variable declaration
                    %%type%%* newData;

                    newData = (%%type%%*) vector->allocator(size * sizeof(%%type%%));
                    if (newData)
                    {
                        // Variable declaration
                        int index;

                        for (index = 0; index < vector->size; index++)
                        {
                            newData[index] = vector->data[index];
                        }
                        free(vector->data);
                        vector->data = newData;
                        toReturn = %%scope%%_Vector_eOk;
                    }
                    else { toReturn = %%scope%%_Vector_eAllocation; }
                }
                else { toReturn = %%scope%%_Vector_eOk; }
            }
            else { toReturn = %%scope%%_Vector_eRange; }
        }
        else { toReturn = %%scope%%_Vector_eRange; }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_capacity(%%scope%%_Vector* vector, int* capacity)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        capacity = vector->capacity;

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
// void %%scope%%_Vector_shrinkToFit(%%scope%%_Vector* vector);
// Modifiers
%%scope%%_Vector_error %%scope%%_Vector_clear(%%scope%%_Vector* vector)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        vector->size = 0;

        toReturn = %%scope%%_Vector_eOk;
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_insert(%%scope%%_Vector* vector, %%type%% item, int index)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        if (index >= 0 && index <= vector->size)
        {
            // Grow the vector, if necessary
            if (vector->size + 1 > vector->capacity)
            {
                toReturn = grow();
            }
            if (toReturn != %%scope%%_Vector_eUndefined && toReturn != %%scope%%_Vector_eAllocation)
            {
                // Variable declaration
                int position;

                // Shift all items after the insertion point
                for (position = vector->size; position > index; position--)
                {
                    vector->data[position] = vector->data[position - 1];
                }

                // Insert new item
                vector->data[index] = item;
                vector->size++;

                toReturn = %%scope%%_Vector_eOk;
            }
            else {
                // No-op...return code already set appropriately
            }
        } else { toReturn = %%scope%%_Vector_eRange; }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_erase(%%scope%%_Vector* vector, int index)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        if (index >= 0 && index < vector->size)
        {
            // Variable declaration
            int position;

            for (position = index + 1; position < vector->size; position++)
            {
                vector->data[position - 1] = vector->data[position];
            }
            vector->size--;

            toReturn = %%scope%%_Vector_eOk;
        }
        else { toReturn = %%scope%%_Vector_eRange; }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_pushBack(%%scope%%_Vector* vector, %%type%% item)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        // Grow the vector, if necessary
        if (vector->size + 1 > vector->capacity)
        {
            toReturn = grow();
        }
        if (toReturn != %%scope%%_Vector_eUndefined && toReturn != %%scope%%_Vector_eAllocation)
        {
            // Insert new item
            vector->data[vector->size] = item;
            vector->size++;

            toReturn = %%scope%%_Vector_eOk;
        }
        else {
            // No-op...return code already set appropriately
        }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_popBack(%%scope%%_Vector* vector)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        if (vector->size > 0)
        {
            vector->size--;

            toReturn = %%scope%%_Vector_eOk;
        }
        else { toReturn = %%scope%%_Vector_eRange; }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Vector_error %%scope%%_Vector_resize(%%scope%%_Vector* vector, int size)
{
    %%scope%%_Vector_error toReturn = %%scope%%_Vector_eUndefined;

    if (vector)
    {
        if (size >= 0)
        {
            if (!%%scope_upper%%_VECTOR_MAX_SIZE || size <= %%scope_upper%%_VECTOR_MAX_SIZE)
            {
                // Remove items if requested size is less than actual size
                if (size < vector->size)
                {
                    vector->size = size;
                }
                else if (size > vector->capacity)
                {
                    toReturn = %%scope%%_Vector_reserve(vector, size);
                }
                else
                {
                    // No-op...vector is already the correct size
                    toReturn = %%scope%%_Vector_eOk;
                }
            }
            else { toReturn = %%scope%%_Vector_eRange; }
        }
        else { toReturn = %%scope%%_Vector_eRange; }
    }
    else { toReturn = %%scope%%_Vector_eInvalidPointer; }

    return toReturn;
}
// void %%scope%%_Vector_swap(%%scope%%_Vector* vector, %%scope%%_Vector* other);
