#ifndef __%%scope_upper%%_ARRAY_GEN_H_
#define __%%scope_upper%%_ARRAY_GEN_H_
%%includes%%
// Type declarations
typedef struct %%scope%%_Array_s %%scope%%_Array;
typedef enum %%scope%%_Array_error_e %%scope%%_Array_error;

// Function declarations
// Allocation
%%scope%%_Array_error %%scope%%_Array_init(%%scope%%_Array* array, %%type%% type_default);
// Accessors
%%scope%%_Array_error %%scope%%_Array_at(%%scope%%_Array* array, int index, %%type%%* reference);
%%scope%%_Array_error %%scope%%_Array_front(%%scope%%_Array* array, %%type%%* reference);
%%scope%%_Array_error %%scope%%_Array_back(%%scope%%_Array* array, %%type%%* reference);
// Capacity
%%scope%%_Array_error %%scope%%_Array_empty(%%scope%%_Array* array, int* result);
%%scope%%_Array_error %%scope%%_Array_size(%%scope%%_Array* array, int* size);
// int %%scope%%_Array_maxSize(%%scope%%_Array* array);
// Modifiers
// void %%scope%%_Array_swap(%%scope%%_Array* array, %%scope%%_Array* other);
%%scope%%_Array_error %%scope%%_Array_fill(%%scope%%_Array* array, %%type%% item);

// Type definitions
struct %%scope%%_Array_s
{
    int count;
    %%type%% data[%%capacity%%];
};
enum %%scope%%_Array_error_e
{
    %%scope%%_Array_eUndefined,
    %%scope%%_Array_eOk,
    %%scope%%_Array_eInvalidPointer,
    %%scope%%_Array_eRange
};

#endif
