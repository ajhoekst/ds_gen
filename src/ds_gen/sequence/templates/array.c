#include "%%scope%%_Array_gen.h"

// Function definitions
// Allocation
%%scope%%_Array_error %%scope%%_Array_init(%%scope%%_Array* array, %%type%% type_default)
{
    %%scope%%_Array_error toReturn = %%scope%%_Array_eUndefined;

    if (array)
    {
        array->count = 0;
        toReturn = %%scope%%_Array_fill(array, type_default);
    }
    else { toReturn = %%scope%%_Array_eInvalidPointer; }

    return toReturn;
}
// Accessors
%%scope%%_Array_error %%scope%%_Array_at(%%scope%%_Array* array, int index, %%type%%* reference)
{
    %%scope%%_Array_error toReturn = %%scope%%_Array_eUndefined;

    if (array)
    {
        if (index >=0 && index < %%capacity%%)
        {
            reference = &array->data[index];

            toReturn = %%scope%%_Array_eOk;
        }
        else { toReturn = %%scope%%_Array_eRange; }
    }
    else { toReturn = %%scope%%_Array_eInvalidPointer; }

    return toReturn;
}
// Capacity
%%scope%%_Array_error %%scope%%_Array_empty(%%scope%%_Array* array, int* result)
{
    %%scope%%_Array_error toReturn = %%scope%%_Array_eUndefined;

    if (array)
    {
        result = array->count == 0;

        toReturn = %%scope%%_Array_eOk;
    }
    else { toReturn = %%scope%%_Array_eInvalidPointer; }

    return toReturn;
}
%%scope%%_Array_error %%scope%%_Array_size(%%scope%%_Array* array, int* size)
{
    %%scope%%_Array_error toReturn = %%scope%%_Array_eUndefined;

    if (array)
    {
        *size = %%capacity%%;

        toReturn = %%scope%%_Array_eOk;
    }
    else { toReturn = %%scope%%_Array_eInvalidPointer; }

    return toReturn;
}
// int %%scope%%_Array_maxSize(%%scope%%_Array* array);
// Modifiers
// void %%scope%%_Array_swap(%%scope%%_Array* array, %%scope%%_Array* other);
%%scope%%_Array_error %%scope%%_Array_fill(%%scope%%_Array* array, %%type%% item)
{
    %%scope%%_Array_error toReturn = %%scope%%_Array_eUndefined;

    if (array)
    {
        // Variable declaration
        int index;

        for (index = 0; index < %%capacity%%; index++)
        {
            array->data[index] = item;
        }

        toReturn = %%scope%%_Array_eOk;
    }
    else { toReturn = %%scope%%_Array_eInvalidPointer; }

    return toReturn;
}
