#ifndef __%%scope_upper%%_VECTOR_H_
#define __%%scope_upper%%_VECTOR_H_
%%includes%%
#include <stddef.h>

#define %%scope_upper%%_VECTOR_MAX_SIZE %%max%%

// Type declarations
typedef struct %%scope%%_Vector_s %%scope%%_Vector;
typedef enum %%scope%%_Vector_error_e %%scope%%_Vector_error;

// Function declarations
// Allocation
%%scope%%_Vector_error %%scope%%_Vector_init(%%scope%%_Vector* vector, void* (*allocator)(size_t size));
// Accessors
%%scope%%_Vector_error %%scope%%_Vector_at(%%scope%%_Vector* vector, int index, %%type%%* reference);
%%scope%%_Vector_error %%scope%%_Vector_front(%%scope%%_Vector* vector, %%type%%* reference);
%%scope%%_Vector_error %%scope%%_Vector_back(%%scope%%_Vector* vector, %%type%%* reference);
// Capacity
%%scope%%_Vector_error %%scope%%_Vector_empty(%%scope%%_Vector* vector, int* result);
%%scope%%_Vector_error %%scope%%_Vector_size(%%scope%%_Vector* vector, int* size);
// int %%scope%%_Vector_maxSize(%%scope%%_Vector* vector);
%%scope%%_Vector_error %%scope%%_Vector_reserve(%%scope%%_Vector* vector, int size);
%%scope%%_Vector_error %%scope%%_Vector_capacity(%%scope%%_Vector* vector, int* capacity);
// void %%scope%%_Vector_shrinkToFit(%%scope%%_Vector* vector);
// Modifiers
%%scope%%_Vector_error %%scope%%_Vector_clear(%%scope%%_Vector* vector);
%%scope%%_Vector_error %%scope%%_Vector_insert(%%scope%%_Vector* vector, %%type%% item, int index);
%%scope%%_Vector_error %%scope%%_Vector_erase(%%scope%%_Vector* vector, int index);
%%scope%%_Vector_error %%scope%%_Vector_pushBack(%%scope%%_Vector* vector, %%type%% item);
%%scope%%_Vector_error %%scope%%_Vector_popBack(%%scope%%_Vector* vector);
%%scope%%_Vector_error %%scope%%_Vector_resize(%%scope%%_Vector* vector, int size);
// void %%scope%%_Vector_swap(%%scope%%_Vector* vector, %%scope%%_Vector* other);

// Type definitions
struct %%scope%%_Vector_s
{
    int capacity;
    int size;
    void* (*allocator)(size_t size);
    %%type%%* data;
};
enum %%scope%%_Vector_error_e
{
    %%scope%%_Vector_eUndefined,
    %%scope%%_Vector_eOk,
    %%scope%%_Vector_eInvalidPointer,
    %%scope%%_Vector_eAllocation,
    %%scope%%_Vector_eRange
};

#endif
