import functools
import os

from ..common import check_arg_natural_number

def add_vector_subparsers(add_parent_args, parent_handler, parent_subparsers):
    # Add parser
    parser = parent_subparsers.add_parser('vector')
    # Add arguments
    add_vector_args(add_parent_args, parser)
    # Set command defaults
    parser.set_defaults(
        file=__file__,
        func=functools.partial(handle_vector_args, parent_handler)
    )

def add_vector_args(add_parent_args, parser):
    # Add parent arguments
    add_parent_args(parser)

    # Add vector-related arguments
    parser.add_argument(
        '-c',
        '--capacity',
        nargs='?',
        default=16,
        type=functools.partial(check_arg_natural_number, 'Capacity'),
        help='Initial capacity of the vector'
    )
    parser.add_argument(
        '-m',
        '--max',
        nargs='?',
        default=0,
        type=functools.partial(check_arg_natural_number, 'Maximum capacity'),
        help='Maximum capacity of the vector'
    )

def handle_vector_args(parent_handler, args, template):
    # Replace sequence-related tags
    updated_template = parent_handler(args, template)

    # Replace vector-specific tags
    updated_template = updated_template.replace(
        '%%capacity%%',
        str(args.capacity)
    )
    updated_template = updated_template.replace(
        '%%max%%',
        str(args.max)
    )

    return updated_template
