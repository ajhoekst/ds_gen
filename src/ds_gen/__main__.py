import argparse
import functools
import os

from .common import add_common_args
from .common import handle_common_args
from .common import data_structure_generator
from .queue import add_queue_subparsers
from .sequence import add_sequence_subparsers

def create_parser():
    # Top-level parser
    ds_gen_parser = argparse.ArgumentParser(
        prog="ds_gen",
        description="Program to generate C source for various data structures"
    )
    ds_gen_subparsers = ds_gen_parser.add_subparsers()

    # Define subparsers
    subparsers = [
        add_queue_subparsers,
        add_sequence_subparsers
    ]

    # Add subparsers
    for subparser in subparsers:
        functools.partial(
            subparser,
            add_common_args,
            handle_common_args,
            ds_gen_subparsers
        )()

    return ds_gen_parser

# Entrypoint
if __name__ == '__main__':
    args = create_parser().parse_args()

    try:
        data_structure_generator(args.func, args)
    except AttributeError:
        pass
