import functools
import os

from ..common import check_arg_natural_number

def add_circular_buffer_subparsers(add_parent_args, parent_handler, parent_subparsers):
    # Add parser
    parser = parent_subparsers.add_parser('circular_buffer')
    # Add arguments
    add_circular_buffer_args(add_parent_args, parser)
    # Set command defaults
    parser.set_defaults(
        file=__file__,
        func=functools.partial(handle_circular_buffer_args, parent_handler)
    )

def add_circular_buffer_args(add_parent_args, parser):
    # Add parent arguments
    add_parent_args(parser)

    # Add circular-buffer-related arguments
    parser.add_argument(
        '-c',
        '--capacity',
        nargs='?',
        default=16,
        type=functools.partial(check_arg_natural_number, 'Capacity'),
        help='Capacity of the circular buffer'
    )

def handle_circular_buffer_args(parent_handler, args, template):
    # Replace queue-related tags
    updated_template = parent_handler(args, template)

    # Replace circular-buffer-related tags
    updated_template = updated_template.replace(
        '%%capacity%%',
        str(args.capacity)
    )

    return updated_template
