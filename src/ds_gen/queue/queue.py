import functools

from .CircularBuffer import add_circular_buffer_subparsers

def add_queue_subparsers(add_parent_args, parent_handler, parent_subparsers):
    # Add parser
    parser = parent_subparsers.add_parser('queue')
    queue_subparsers = parser.add_subparsers()

    # Define subparsers
    subparsers = [
        add_circular_buffer_subparsers
    ]

    # Add subparsers
    for subparser in subparsers:
        functools.partial(
            subparser,
            functools.partial(add_queue_args, add_parent_args),
            functools.partial(handle_queue_args, parent_handler),
            queue_subparsers
        )()

def add_queue_args(add_parent_args, parser):
    # Add parent arguments
    add_parent_args(parser)

    # Add queue-related arguments
    parser.add_argument(
        'type',
        nargs='?',
        default='int',
        type=str,
        help='The type being stored in the queue'
    )
    parser.add_argument(
        'type_header',
        nargs='?',
        default=None,
        type=str,
        help='The header file declaring the type being stored in the queue'
    )

def handle_queue_args(parent_handler, args, template):
    # Process parent arguments
    updated_template = parent_handler(args, template)
    
    # Replace type tags
    updated_template = updated_template.replace(
        '%%type%%',
        args.type
    )
    
    # Replace includes tag
    if args.type_header:
        updated_template = updated_template.replace(
            '%%includes%%',
            '\n#include "' + args.type_header + '"\n'
        )
    else:
        updated_template = updated_template.replace(
            '%%includes%%',
            ''
        )
    
    return updated_template
