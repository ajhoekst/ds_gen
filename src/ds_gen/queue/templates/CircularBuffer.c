#include "%%scope%%_CircularBuffer_gen.h"

// Function declarations
inline static void %%scope%%_CircularBuffer_increment(int* value);

// Function definitions
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_init(%%scope%%_CircularBuffer* queue)
{
    %%scope%%_CircularBuffer_error toReturn = %%scope%%_CircularBuffer_eUndefined;

    if (queue)
    {
        queue->readPtr = 0;
        queue->writePtr = 0;
        queue->isEmpty = 1;
        queue->isFull = 0;
        
        toReturn = %%scope%%_CircularBuffer_eOk;
    }
    else { toReturn = %%scope%%_CircularBuffer_eInvalidPointer; }

    return toReturn;
}
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_enqueue(%%scope%%_CircularBuffer* queue, %%type%% item)
{
    %%scope%%_CircularBuffer_error toReturn = %%scope%%_CircularBuffer_eUndefined;

    if (queue)
    {
        if (!queue->isFull)
        {
            queue->data[queue->writePtr] = item;
            %%scope%%_CircularBuffer_increment(&queue->writePtr);
            queue->isEmpty = 0;
            if (queue->writePtr == queue->readPtr)
            {
                queue->isFull = 1;
            }
            else { /* No-op */ }

            toReturn = %%scope%%_CircularBuffer_eOk;
        }
        else { toReturn = %%scope%%_CircularBuffer_eRange; }
    }
    else { toReturn = %%scope%%_CircularBuffer_eInvalidPointer; }

    return toReturn;
}
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_dequeue(%%scope%%_CircularBuffer* queue, %%type%%* item)
{
    %%scope%%_CircularBuffer_error toReturn = %%scope%%_CircularBuffer_eUndefined;

    if (queue)
    {
        if (!queue->isEmpty)
        {
            *item = queue->data[queue->readPtr];
            %%scope%%_CircularBuffer_increment(&queue->readPtr);
            queue->isFull = 0;
            if (queue->writePtr == queue->readPtr)
            {
                queue->isEmpty = 1;
            }
            toReturn = %%scope%%_CircularBuffer_eOk;
        }
        else { toReturn = %%scope%%_CircularBuffer_eRange; }
    }
    else { toReturn = %%scope%%_CircularBuffer_eInvalidPointer; }

    return toReturn;
}
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_isEmpty(%%scope%%_CircularBuffer* queue, unsigned char* result)
{
    %%scope%%_CircularBuffer_error toReturn = %%scope%%_CircularBuffer_eUndefined;

    if (queue)
    {
        *result = queue->isEmpty;

        toReturn = %%scope%%_CircularBuffer_eOk;
    }
    else { toReturn = %%scope%%_CircularBuffer_eInvalidPointer; }

    return toReturn;
}
void %%scope%%_CircularBuffer_increment(int* value)
{
    *value = (*value + 1) % %%capacity%%;
}
