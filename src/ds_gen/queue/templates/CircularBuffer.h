#ifndef __%%scope_upper%%_CIRCULAR_BUFFER_GEN_H_
#define __%%scope_upper%%_CIRCULAR_BUFFER_GEN_H_
%%includes%%
// Type declarations
typedef struct %%scope%%_CircularBuffer_s %%scope%%_CircularBuffer;
typedef enum %%scope%%_CircularBuffer_error_e %%scope%%_CircularBuffer_error;

// Function declarations
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_init(%%scope%%_CircularBuffer* queue);
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_enqueue(%%scope%%_CircularBuffer* queue, %%type%% item);
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_dequeue(%%scope%%_CircularBuffer* queue, %%type%%* item);
%%scope%%_CircularBuffer_error %%scope%%_CircularBuffer_isEmpty(%%scope%%_CircularBuffer* queue, unsigned char* result);

// Type definitions
struct %%scope%%_CircularBuffer_s
{
    int readPtr;
    int writePtr;
    unsigned char isEmpty : 1;
    unsigned char isFull : 1;
    unsigned char unused : 5;
    %%type%% data[%%capacity%%];
};
enum %%scope%%_CircularBuffer_error_e
{
    %%scope%%_CircularBuffer_eUndefined,
    %%scope%%_CircularBuffer_eOk,
    %%scope%%_CircularBuffer_eInvalidPointer,
    %%scope%%_CircularBuffer_eRange
};

#endif
