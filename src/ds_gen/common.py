import argparse
import functools
import os

def add_common_args(parser):
    parser.add_argument('--scope', nargs='?', default='', type=str)
    parser.add_argument('--header-out', nargs='?', default='.', type=str)
    parser.add_argument('--source-out', nargs='?', default='.', type=str)

def handle_common_args(args, template):
    # Replace scope tags that must be all caps
    updated_template = template.replace(
        '%%scope_upper%%',
        str.upper(args.scope)
    )
    
    # Replace scope tags
    updated_template = updated_template.replace(
        '%%scope%%',
        args.scope
    )

    return updated_template

def check_arg_natural_number(name, value):
    try:
        toReturn = int(float(value))
    except:
        raise argparse.ArgumentTypeError("{} must be a number (given {})".format(name, value))

    if toReturn <= 0:
        raise argparse.ArgumentTypeError("{} must be non-negative (given {})".format(name, value))

    return toReturn

def write_file(template, file_out, func):
    with open(template, 'r') as t:
        with open(file_out, 'w') as f:
            for line in t:
                f.write(func(line))

def data_structure_generator(func, args):
    ds_name = args.file.split('/')[-1].split('.')[0]
    template_dir = os.path.join(os.path.dirname(args.file), 'templates')

    # Write header file
    write_file(
        os.path.join(template_dir, '{}.h'.format(ds_name)),
        os.path.join(args.header_out, '{}_{}_gen.h'.format(args.scope, ds_name)),
        functools.partial(func, args)
    )
    
    # Write implementation file
    write_file(
        os.path.join(template_dir, '{}.c'.format(ds_name)),
        os.path.join(args.source_out, '{}_{}_gen.c'.format(args.scope, ds_name)),
        functools.partial(func, args)
    )
