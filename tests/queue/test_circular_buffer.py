import pytest

from argparse import Namespace

from ds_gen.queue.CircularBuffer import handle_circular_buffer_args

parent_identity = lambda args, template: template
base_args = Namespace(
    capacity=1,
    header_out='.',
    source_out='.'
)

def test_handle_circular_buffer_args_no_tag():
    assert handle_circular_buffer_args(
        parent_identity,
        base_args,
        'test'
    ) == 'test'

def test_handle_circular_buffer_args_single_capacity_tag():
    assert handle_circular_buffer_args(
        parent_identity,
        base_args,
        '%%capacity%%'
    ) == '1'

def test_handle_circular_buffer_args_multiple_capacity_tags():
    assert handle_circular_buffer_args(
        parent_identity,
        base_args,
        '%%capacity%%ignore%%capacity%%'
    ) == '1ignore1'
