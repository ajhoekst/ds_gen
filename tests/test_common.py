import argparse
import pytest

from ds_gen.common import check_arg_natural_number

def test_check_arg_natural_number_with_natural_number():
        assert 1 == check_arg_natural_number('ignore', '1')

def test_check_arg_natural_number_with_floating_point():
    assert 3 == check_arg_natural_number('ignore', '3.4')

def test_check_arg_natural_number_with_non_numeric_string():
    with pytest.raises(argparse.ArgumentTypeError):
        check_arg_natural_number('ignore', 'should fail')

def test_check_arg_natural_number_with_zero():
    with pytest.raises(argparse.ArgumentTypeError):
        check_arg_natural_number('ignore', '0')

def test_check_arg_natural_number_with_negative():
    with pytest.raises(argparse.ArgumentTypeError):
        check_arg_natural_number('ignore', '-1')
