FROM library/python:%%python_tag%%

COPY setup.py /code/setup.py
COPY setup.cfg /code/setup.cfg
COPY MANIFEST.in /code/MANIFEST.in
COPY distribute /code/distribute
COPY clean-generated /code/clean-generated
COPY README.md /code/README.md
COPY src /code/src
COPY tests /code/tests

WORKDIR /code

RUN pip install .
RUN python setup.py test

ARG DISTRIBUTE=0
RUN if [ "${DISTRIBUTE}" -eq "1" ]; then ./distribute; fi;

ENTRYPOINT ["/bin/ash"]
