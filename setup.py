#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

import io
import re
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import find_packages
from setuptools import setup

# def read(*names, **kwargs):
#     return io.open(
#         join(dirname(__file__), *names),
#         encoding=kwargs.get('encoding', 'utf8')
#     ).read()

setup(
    name='ds_gen',
    version='0.0.1',
    description='Python module to generate C source data structures',
    long_description='', # In the future, use read above on README.md and CHANGELOG.md and string replace to generate this
    author='Andrew Hoekstra',
    author_email='connect@andrewhoekstra.com',
    url='https://gitlab.com/ajhoekst/ds_gen',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    zip_safe=False,
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    install_requires=[],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        'Operating System :: MacOS',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: C',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Code Generators',
        'Topic :: Utilities',
        'Natural Language :: English'
    ],
)
